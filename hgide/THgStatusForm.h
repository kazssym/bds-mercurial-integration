/*
 * THgStatusForm - Status form for HgBDS (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef THgStatusFormH
#define THgStatusFormH 1

#include "Hg.hpp"
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Controls.hpp>
#include <ActnList.hpp>
#include <Classes.hpp>

class THgStatusForm : public TForm {
__published:
    TPanel *Panel1;
    TLabel *Label1;
    TListView *FileListView;
    TButton *AddButton;
    TButton *RemoveButton;
    TButton *RevertButton;
    TButton *CommitButton;
    TButton *OKButton;

    TActionList *ActionList1;
    TAction *MercurialAdd;
    TAction *MercurialRemove;
    TAction *MercurialRevert;
    TAction *MercurialCommit;

    void __fastcall FormActivate(TObject *Sender);
    void __fastcall FileListViewSelectItem(TObject *Sender, TListItem *Item,
            bool Selected);
    void __fastcall FileListViewItemChecked(TObject *Sender, TListItem *Item);
    void __fastcall MercurialCommitUpdate(TObject *Sender);
    void __fastcall MercurialCommitExecute(TObject *Sender);

public:
    /**
     * Associated Mercurial repository.
     */
    __property _di_IHgRepository Repository = {
        read = GetRepository, write = SetRepository};

    __fastcall THgStatusForm(TComponent *Owner);

    /**
     * Clears this form.
     */
    void __fastcall Clear();

    /**
     * Refreshes the file list.
     */
    void __fastcall RefreshFileList();

    /**
     * Adds a file to the file list.
     */
    void __fastcall AddFile(THgFile &File, bool Selected = false);

protected:
    /**
     * Getter method for 'Repository'.
     */
    _di_IHgRepository __fastcall GetRepository();

    /**
     * Setter method for 'Repository'.
     */
    void __fastcall SetRepository(_di_IHgRepository Repository);

private:
    _di_IHgRepository FRepository;
};
//---------------------------------------------------------------------------
extern PACKAGE THgStatusForm *HgStatusForm;
//---------------------------------------------------------------------------
#endif
