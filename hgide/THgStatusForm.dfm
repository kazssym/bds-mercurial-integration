object HgStatusForm: THgStatusForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Mercurial Status'
  ClientHeight = 401
  ClientWidth = 601
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  DesignSize = (
    601
    401)
  PixelsPerInch = 96
  TextHeight = 15
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 602
    Height = 361
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Panel1'
    Color = clWindow
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      602
      361)
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 26
      Height = 15
      Caption = '&Files:'
      FocusControl = FileListView
    end
    object FileListView: TListView
      Left = 8
      Top = 24
      Width = 584
      Height = 298
      Anchors = [akLeft, akTop, akRight, akBottom]
      Checkboxes = True
      Columns = <
        item
          Caption = 'Name'
          Width = 160
        end
        item
          Caption = 'Folder path'
          Width = 240
        end>
      Groups = <
        item
          Header = 'Missing'
          GroupID = 6
          State = [lgsNormal, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Removed'
          GroupID = 5
          State = [lgsNormal, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Added'
          GroupID = 4
          State = [lgsNormal, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Modified'
          GroupID = 3
          State = [lgsNormal, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Clean'
          GroupID = 2
          State = [lgsNormal, lgsCollapsed, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Ignored'
          GroupID = 1
          State = [lgsNormal, lgsCollapsed, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Unknown'
          GroupID = 0
          State = [lgsNormal, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end>
      MultiSelect = True
      GroupView = True
      ReadOnly = True
      RowSelect = True
      SortType = stBoth
      TabOrder = 0
      ViewStyle = vsReport
      OnSelectItem = FileListViewSelectItem
      OnItemChecked = FileListViewItemChecked
    end
    object AddButton: TButton
      Left = 248
      Top = 328
      Width = 81
      Height = 25
      Action = MercurialAdd
      Anchors = [akRight, akBottom]
      TabOrder = 1
    end
    object RemoveButton: TButton
      Left = 336
      Top = 328
      Width = 81
      Height = 25
      Action = MercurialRemove
      Anchors = [akRight, akBottom]
      TabOrder = 2
    end
    object RevertButton: TButton
      Left = 424
      Top = 328
      Width = 81
      Height = 25
      Action = MercurialRevert
      Anchors = [akRight, akBottom]
      TabOrder = 3
    end
    object CommitButton: TButton
      Left = 512
      Top = 328
      Width = 81
      Height = 25
      Action = MercurialCommit
      Anchors = [akRight, akBottom]
      TabOrder = 4
    end
  end
  object OKButton: TButton
    Left = 512
    Top = 368
    Width = 81
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object ActionList1: TActionList
    Left = 32
    Top = 32
    object MercurialAdd: TAction
      Category = 'Mercurial'
      Caption = '&Add'
    end
    object MercurialRemove: TAction
      Category = 'Mercurial'
      Caption = '&Remove'
    end
    object MercurialRevert: TAction
      Category = 'Mercurial'
      Caption = 'R&evert'
    end
    object MercurialCommit: TAction
      Category = 'Mercurial'
      Caption = '&Commit...'
      OnExecute = MercurialCommitExecute
      OnUpdate = MercurialCommitUpdate
    end
  end
end
