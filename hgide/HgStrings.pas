{
  HgStrings - string declarations
  Copyright (C) 2012-2014 Kaz Nishimura

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.
}

unit HgStrings;

interface

const
{$IF CompilerVersion >= 27}
  HostProduct = 'RAD Studio XE6';
{$ELSEIF CompilerVersion >= 26}
  HostProduct = 'RAD Studio XE5';
{$ELSEIF CompilerVersion >= 25}
  HostProduct = 'RAD Studio XE4';
{$ELSEIF CompilerVersion >= 24}
  HostProduct = 'RAD Studio XE3';
{$ELSEIF CompilerVersion >= 23}
  HostProduct = 'RAD Studio XE2';
{$ELSEIF CompilerVersion >= 22}
  HostProduct = 'RAD Studio XE';
{ NOTE: An ENDIF directive is valid only for IFDEF, IFNDEF, and IFOPT
  directives before XE4. }
{$IFEND}

resourcestring
  PluginTitle = '%s for ' + HostProduct;
  PluginDescription = '%s %s'#13#10'Copyright '#$00A9' %s Kaz Nishimura';
  PluginLicenseStatus = 'Licensed under the GNU GPL';

  { Letters 'M', 'E', and 'R' are alread used in the Project Manager menu. }
  MercurialMenuCaption = 'Mer&curial';
  StatusMenuCaption = '&Status';
  UpdateCaption = '&Update...';

implementation

end.

