/*
 * HgUtils - utility classes/functions for HgBDS (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HgUtilsH
#define HgUtilsH 1

namespace Hgutils {
    /**
     * Shows a message in the Messages window.
     */
    void __fastcall ShowMessage(const UnicodeString Message);

    /**
     * Shows a debug message in the Messages window.
     * This function does nothing if 'NDEBUG' is defined.
     */
    void __fastcall ShowDebugMessage(const UnicodeString Message);
}
#ifndef DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE
using namespace Hgutils;
#endif

#endif
