/*
 * HgVersionControl - Project Manager integration for HgBDS (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HgVersionControlH
#define HgVersionControlH 1

#include "workaround.h"
#include "Hg.hpp"
#include <ToolsAPI.hpp>

#if __cplusplus >= 201103L
#define OVERRIDE override
#else
#define OVERRIDE
#endif

namespace Hgversioncontrol {
    /*
     * Version control notifier for Mercurial.
     */
    class THgVersionControlNotifier : public TInterfacedObject,
        private IOTAVersionControlNotifier150,
        private IOTAVersionControlNotifier
    {
        typedef TInterfacedObject inherited;

    public:
        /**
         * Parent menu item for all the Mercurial commands.
         */
        __property _di_IOTAProjectManagerMenu MercurialMenu = {
            read = GetMercurialMenu,
        };

        /**
         * Menu item to show the Mercurial Status form.
         */
        __property _di_IOTAProjectManagerMenu StatusMenu = {
            read = GetStatusMenu,
        };

        __fastcall THgVersionControlNotifier(
            _di_IHgVersionControlService Service);

        /**
         * Event handler for 'StatusMenu'.
         */
        void __fastcall StatusMenuExecute(TObject *Sender,
            _di_IInterfaceList MenuContextList);

        // 'IOTAVersionControlNotifier150' methods
        bool __fastcall CheckoutProject(UnicodeString &ProjectName) OVERRIDE;
        bool __fastcall CheckoutProjectWithConnection(
            UnicodeString &ProjectName, const UnicodeString Connection)
            OVERRIDE;
        UnicodeString __fastcall GetName() OVERRIDE;

        // 'IOTAVersionControlNotifier' methods
        UnicodeString __fastcall GetDisplayName() OVERRIDE;
        bool __fastcall IsFileManaged(const _di_IOTAProject Project,
            TStrings *const IdentList) /* OVERRIDE */;
        void __fastcall ProjectManagerMenu(const _di_IOTAProject Project,
            TStrings *const IdentList,
            const _di_IInterfaceList ProjectManagerMenuList,
            bool IsMultiSelect) /* OVERRIDE */;
        bool __fastcall AddNewProject(const _di_IOTAProject Project)
            OVERRIDE;

#if __BORLANDC__ < 0x650 /* Before XE3 */
        // Old signature methods that are used before XE3.

        bool __fastcall IsFileManaged(const _di_IOTAProject Project,
            const TStrings *IdentList) OVERRIDE
        {
            return IsFileManaged(Project, const_cast<TStrings *>(IdentList));
        }

        void __fastcall ProjectManagerMenu(const _di_IOTAProject Project,
            const TStrings *IdentList,
            const _di_IInterfaceList ProjectManagerMenuList,
            bool IsMultiSelect) OVERRIDE
        {
            ProjectManagerMenu(Project, const_cast<TStrings *>(IdentList),
                ProjectManagerMenuList, IsMultiSelect);
        }
#endif

        // 'IOTANotifier' methods

        void __fastcall AfterSave() OVERRIDE {
        }

        void __fastcall BeforeSave() OVERRIDE {
        }

        void __fastcall Destroyed() OVERRIDE {
        }

        void __fastcall Modified() OVERRIDE {
        }

        // 'IUnknown' methods
        INTFOBJECT_IMPL_IUNKNOWN(inherited);

    protected:
        /**
         * Getter method for 'MercurialMenu'.
         */
        _di_IOTAProjectManagerMenu __fastcall GetMercurialMenu();

        /**
         * Getter method for 'StatusMenu'.
         */
        _di_IOTAProjectManagerMenu __fastcall GetStatusMenu();

    private:
        _di_IHgVersionControlService FVersionControlService;
        _di_IOTAProjectManagerMenu FMercurialMenu;
        _di_IOTAProjectManagerMenu FStatusMenu;
        _di_IOTAProjectManagerMenu UpdateMenu;
    };
}
using namespace Hgversioncontrol;

#endif
