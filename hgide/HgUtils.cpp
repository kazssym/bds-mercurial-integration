/*
 * HgUtils - utility classes/functions for HgBDS (implementation)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <basepch.h>
#pragma hdrstop

#include "HgUtils.h"

#include <ToolsAPI.hpp>

#pragma package(smart_init)

void __fastcall Hgutils::ShowMessage(const UnicodeString Message) {
    _di_IOTAMessageServices messageServices;
    if (BorlandIDEServices->Supports(messageServices)) {
        _di_IOTAMessageGroup messageGroup =
                messageServices->AddMessageGroup(L"Mercurial");
        messageGroup->AutoScroll = true;
        messageServices->AddTitleMessage(Message, messageGroup);
    }
}

void __fastcall Hgutils::ShowDebugMessage(const UnicodeString Message) {
#ifndef NDEBUG
    ShowMessage(Message);
#endif
}
