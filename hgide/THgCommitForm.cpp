/*
 * THgCommitForm - commit form for HgBDS (implementation)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <basepch.h>
#pragma hdrstop

#include "THgCommitForm.h"

#include <IOUtils.hpp>

#pragma package(smart_init)
#pragma resource "*.dfm"
THgCommitForm *HgCommitForm;

__fastcall THgCommitForm::THgCommitForm(TComponent *Owner) : TForm(Owner) {
}

void __fastcall THgCommitForm::Clear() {
    FileListView->Clear();
    MessageEdit->Clear();
}

void __fastcall THgCommitForm::AddFile(THgFile &File, bool Selected) {
    UnicodeString folderPath = TPath::GetDirectoryName(File.Name);
    if (folderPath.IsEmpty()) {
        folderPath = _D(".");
    }

    FileListView->Items->BeginUpdate();
    try {
        TListItem *newItem = FileListView->Items->Add();
        newItem->Caption = TPath::GetFileName(File.Name);
        newItem->SubItems->Add(folderPath);
        newItem->GroupID = File.Status;
        newItem->Checked = Selected;
    } catch (...) {
        FileListView->Items->EndUpdate();
        throw;
    }
    FileListView->Items->EndUpdate();
}

void __fastcall THgCommitForm::FormOKUpdate(TObject *Sender) {
    bool enabled = false;
    for (int i = 0; i != MessageEdit->Lines->Count; ++i) {
        UnicodeString line = MessageEdit->Lines->Strings[i];
        if (Trim(line) != UnicodeString()) {
            enabled = true;
            break;
        }
    }
    FormOK->Enabled = enabled;
}
