/*
 * THgStatusForm - Status form for HgBDS (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <basepch.h>
#pragma hdrstop

#include "THgStatusForm.h"

#include "THgCommitForm.h"
#include <IOUtils.hpp>
#include <vector>

using namespace std;

#pragma package(smart_init)
#pragma resource "*.dfm"
THgStatusForm *HgStatusForm;

__fastcall THgStatusForm::THgStatusForm(TComponent *Owner)
        : TForm(Owner) {
}

_di_IHgRepository __fastcall THgStatusForm::GetRepository() {
    return FRepository;
}

void __fastcall THgStatusForm::SetRepository(_di_IHgRepository Repository) {
    if (FRepository != Repository) {
        FRepository = Repository;
    }
}

void __fastcall THgStatusForm::Clear() {
    FileListView->Clear();
}

void __fastcall THgStatusForm::RefreshFileList() {
    Clear();
    if (Repository) {
        THgFiles files = Repository->GetFiles();
        for (int i = 0; i != files.Length; ++i) {
            AddFile(files[i], files[i].Status > THgFileStatus::Clean);
        }
    }
}

void __fastcall THgStatusForm::AddFile(THgFile &File, bool Selected) {
    UnicodeString folderPath = TPath::GetDirectoryName(File.Name);
    if (folderPath.IsEmpty()) {
        folderPath = _D(".");
    }

    FileListView->Items->BeginUpdate();
    __try {
        TListItem *newItem = FileListView->Items->Add();
        newItem->Caption = TPath::GetFileName(File.Name);
        newItem->SubItems->Add(folderPath);
        newItem->GroupID = File.Status;
        newItem->Checked = Selected;
    } __finally {
        FileListView->Items->EndUpdate();
    }
}

void __fastcall THgStatusForm::FormActivate(TObject *Sender) {
    RefreshFileList();
}

void __fastcall THgStatusForm::FileListViewSelectItem(TObject *Sender,
        TListItem *Item, bool Selected) {
    // A selected item MUST also be checked.
    Item->Checked = Selected;
}

void __fastcall THgStatusForm::FileListViewItemChecked(TObject *Sender,
        TListItem *Item) {
    // A checked item MUST also be selected.
    Item->Selected = Item->Checked;
}

void __fastcall THgStatusForm::MercurialCommitUpdate(TObject *Sender) {
    bool enabled = false;
    if (FileListView->SelCount != 0) {
        int itemsCount = FileListView->Items->Count;
        for (int i = 0; i != itemsCount; ++i) {
            TListItem *item = FileListView->Items->Item[i];
            if (item->GroupID == THgFileStatus::Missing) {
                // If any file is missing, we cannot commit.
                enabled = false;
                break;
            }
            if (item->Selected) {
                if (item->GroupID <= THgFileStatus::Clean) {
                    // If any unmanaged or clean file is selected, we cannot
                    // commit.
                    enabled = false;
                    break;
                } else {
                    enabled = true;
                }
            }
        }
    }
    MercurialCommit->Enabled = enabled;
}

void __fastcall THgStatusForm::MercurialCommitExecute(TObject *Sender) {
    HgCommitForm = NULL;
    __try {
        vector<UnicodeString> fileNames;

        HgCommitForm = new THgCommitForm(NULL);
        HgCommitForm->FileListView->Items->BeginUpdate();
        __try {
            HgCommitForm->FileListView->Items->Clear();

            int itemsCount = FileListView->Items->Count;
            for (int i = 0; i != itemsCount; ++i) {
                TListItem *item = FileListView->Items->Item[i];
                if (item->GroupID > THgFileStatus::Clean && item->Selected)
                {
                    UnicodeString fileName = item->Caption;
                    fileName = TPath::Combine(item->SubItems->Strings[0],
                        fileName);
                    fileNames.push_back(fileName);

                    TListItem *newItem =
                        HgCommitForm->FileListView->Items->Add();
                    newItem->Assign(item);
                    newItem->GroupID = item->GroupID;
                }
            }
        } __finally {
            HgCommitForm->FileListView->Items->EndUpdate();
        }

        HgCommitForm->ShowModal();
        if (HgCommitForm->ModalResult == mrOk) {
            TStrings *messageLines = HgCommitForm->MessageEdit->Lines;
            FRepository->Commit(messageLines->Text, &fileNames[0],
                fileNames.size() - 1, THgCommitOptions());
            RefreshFileList();
        }
    } __finally {
        delete HgCommitForm;
    }
}
