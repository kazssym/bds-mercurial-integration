/*
 * Workaround definitions for buggy systems
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef workaroundH
#define workaroundH 1

#include <systobj.h>

// Workarounds the RAD Studio IDE violates reference counting
template<>
class RTL_DELPHIRETURN System::DelphiInterface<IInterfaceList> {
private:
    IInterfaceList *intf;

public:
    __fastcall DelphiInterface<IInterfaceList>() : intf(0) {
    }

    __fastcall DelphiInterface<IInterfaceList>
        (const DelphiInterface<IInterfaceList> &rhs) {
        intf = rhs.intf;
    }

    __fastcall DelphiInterface<IInterfaceList>(IInterfaceList *rhs) {
        intf = rhs;
    }

    __fastcall ~DelphiInterface<IInterfaceList>() {
        if (intf != 0) {
            intf = 0;
        }
    }

    DelphiInterface<IInterfaceList>& __fastcall operator =
        (const DelphiInterface<IInterfaceList>& rhs) {
        intf = rhs.intf;
        return *this;
    }

    DelphiInterface<IInterfaceList>& __fastcall operator =(IInterfaceList * rhs)
    {
        intf = rhs;
        return *this;
    }

    IInterfaceList *__fastcall operator ->() const {
        return intf;
    }

    bool operator ! () const {
        return (intf == 0);
    };

    __fastcall operator IInterfaceList *() const {
        return intf;
    }

    IInterfaceList &__fastcall operator *() {
        return *intf;
    }

    int Release() {
        intf = 0;
        return 0;
    }

    IInterfaceList **__fastcall operator &() {
        return &intf;
    }
};

#endif
