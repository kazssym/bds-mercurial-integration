/*
 * HgWizard - wizard registration (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HgWizardH
#define HgWizardH 1

#include "HgClient.h"
#include <FileHistoryAPI.hpp>
#include <ToolsAPI.hpp>

#if __cplusplus >= 201103L
#define OVERRIDE override
#else
#define OVERRIDE
#endif

namespace Hgwizard {
    /*
     * Implements the 'IOTAWizard' interface for this package.
     */
    class THgWizard : public TInterfacedObject, public IOTAWizard {
        typedef TInterfacedObject inherited;

    public:
        __fastcall THgWizard();
        virtual __fastcall ~THgWizard();

        void __fastcall AfterConstruction() OVERRIDE;

        // IOTAWizard methods.
        UnicodeString __fastcall GetIDString() OVERRIDE;
        UnicodeString __fastcall GetName() OVERRIDE;
        TWizardState __fastcall GetState() OVERRIDE;
        void __fastcall Execute() OVERRIDE;

        // IOTANotifier methods.
        void __fastcall AfterSave() OVERRIDE;
        void __fastcall BeforeSave() OVERRIDE;
        void __fastcall Destroyed() OVERRIDE;
        void __fastcall Modified() OVERRIDE;

        // IUnknown methods.
        INTFOBJECT_IMPL_IUNKNOWN(TInterfacedObject);

    private:
        THgClientFactory *FHgClientFactory;

        int PluginInfoIndex;
        int VersionControlNotifierIndex;
        int FileHistoryProviderIndex;
    };

    /*
     * Registers the Wizard.
     */
    void __fastcall PACKAGE Register();
}
using namespace Hgwizard;

#endif
