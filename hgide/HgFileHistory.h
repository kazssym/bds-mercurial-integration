/*
 * HgFileHistory - History Manager integration for HgBDS (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HgFileHistoryH
#define HgFileHistoryH 1

#include "Hg.hpp"
#include <FileHistoryAPI.hpp>

#if __cplusplus >= 201103L
#define OVERRIDE override
#else
#define OVERRIDE
#endif

namespace Hgfilehistory {
    /*
     * File history provider for Mercurial.
     */
    class THgFileHistoryProvider : public TInterfacedObject,
        private IOTAFileHistoryProvider
    {
        typedef TInterfacedObject inherited;

    public:
        __fastcall THgFileHistoryProvider(_di_IHgVersionControlService Hg);

        UnicodeString __fastcall GetIdent();
        UnicodeString __fastcall GetName();
        _di_IOTAFileHistory __fastcall GetFileHistory(UnicodeString FileName);

        // 'IOTAFileHistoryProvider' methods.

        HRESULT __stdcall Get_Ident(WideString &Value) OVERRIDE {
            Value = GetIdent();
            return S_OK;
        }

        HRESULT __stdcall Get_Name(WideString &Value) OVERRIDE {
            Value = GetName();
            return S_OK;
        }

        HRESULT __stdcall GetFileHistory(WideString FileName,
            _di_IOTAFileHistory &FileHistory) OVERRIDE
        {
            FileHistory = GetFileHistory(FileName);
            return S_OK;
        }

        // 'IDispatch' methods.

        HRESULT __stdcall GetTypeInfoCount(UINT *pctinfo) OVERRIDE {
            return E_NOTIMPL;
        }

        HRESULT __stdcall GetTypeInfo(UINT iTInfo, LCID lcid,
            ITypeInfo **ppTInfo) OVERRIDE
        {
            return E_NOTIMPL;
        }

        HRESULT __stdcall GetIDsOfNames(REFIID riid, LPOLESTR *rgszNames,
            UINT cNames, LCID lcid, DISPID *rgDispId) OVERRIDE
        {
            return E_NOTIMPL;
        }

        HRESULT __stdcall Invoke(DISPID dispIdMember, REFIID riid, LCID lcid,
            WORD wFlags, DISPPARAMS *pDispParams, VARIANT *pVarResult,
            EXCEPINFO *pExcepInfo, UINT *puArgErr) OVERRIDE
        {
            return E_NOTIMPL;
        }

        // 'IUnknown' methods.
        INTFOBJECT_IMPL_IUNKNOWN(TInterfacedObject);

    private:
        _di_IHgVersionControlService FHg;
    };
}
using namespace Hgfilehistory;

#endif
