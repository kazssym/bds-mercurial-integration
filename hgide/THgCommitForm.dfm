object HgCommitForm: THgCommitForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Mercurial Commit'
  ClientHeight = 401
  ClientWidth = 601
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    601
    401)
  PixelsPerInch = 96
  TextHeight = 15
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 602
    Height = 361
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Panel1'
    Color = clWindow
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      602
      361)
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 85
      Height = 15
      Caption = '&Files to commit:'
      FocusControl = FileListView
    end
    object Label2: TLabel
      Left = 255
      Top = 8
      Width = 49
      Height = 15
      Caption = '&Message:'
      FocusControl = MessageEdit
    end
    object FileListView: TListView
      Left = 8
      Top = 24
      Width = 241
      Height = 329
      Anchors = [akLeft, akTop, akBottom]
      Columns = <
        item
          Caption = 'Name'
          Width = 120
        end
        item
          AutoSize = True
          Caption = 'Folder path'
        end>
      Groups = <
        item
          Header = 'Missing'
          GroupID = 6
          State = [lgsNormal, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Removed'
          GroupID = 5
          State = [lgsNormal, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Added'
          GroupID = 4
          State = [lgsNormal, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Modified'
          GroupID = 3
          State = [lgsNormal, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Clean'
          GroupID = 2
          State = [lgsNormal, lgsCollapsed, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Ignored'
          GroupID = 1
          State = [lgsNormal, lgsCollapsed, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end
        item
          Header = 'Unknown'
          GroupID = 0
          State = [lgsNormal, lgsCollapsible]
          HeaderAlign = taLeftJustify
          FooterAlign = taLeftJustify
          TitleImage = -1
        end>
      GroupView = True
      ReadOnly = True
      RowSelect = True
      SortType = stBoth
      TabOrder = 0
      ViewStyle = vsReport
    end
    object MessageEdit: TMemo
      Left = 256
      Top = 24
      Width = 338
      Height = 329
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 1
    end
  end
  object OKButton: TButton
    Left = 424
    Top = 368
    Width = 81
    Height = 25
    Action = FormOK
    Anchors = [akRight, akBottom]
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelButton: TButton
    Left = 512
    Top = 368
    Width = 81
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object ActionList1: TActionList
    Left = 32
    Top = 32
    object FormOK: TAction
      Category = 'Form'
      Caption = 'OK'
      OnUpdate = FormOKUpdate
    end
  end
end
