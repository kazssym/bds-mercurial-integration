/*
 * HgFileHistory - History Manager integration for HgBDS (implementation)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <basepch.h>
#pragma hdrstop

#include "HgFileHistory.h"

#include "HgUtils.h"
#include <RTLConsts.hpp>
#include <XMLDoc.hpp>
#include <XSBuiltIns.hpp>
#include <IOUtils.hpp>
#include <vector>
#include <cassert>

/*
 * These macros are renamed in later C++Builder versions.
 */
#ifndef Rtlconsts_SArgumentOutOfRange
#define Rtlconsts_SArgumentOutOfRange System_Rtlconsts_SArgumentOutOfRange
#endif
#ifndef Rtlconsts_SArgumentNil
#define Rtlconsts_SArgumentNil System_Rtlconsts_SArgumentNil
#endif

using namespace std;

#pragma package(smart_init)

namespace Hgfilehistory {
    /*
     * File history for Mercurial.
     */
    class THgFileHistory : public TInterfacedObject, private IOTAFileHistory {
        typedef TInterfacedObject inherited;

    public:
        __property UnicodeString FileName = {read = FFileName};

        __fastcall THgFileHistory(const UnicodeString FileName,
                _di_IHgVersionControlService Hg);

        template<typename InputIterator>
        void __fastcall AddEntry(const UnicodeString ident,
                const UnicodeString author, const TDateTime date,
                const UnicodeString comment, InputIterator labelsFrom,
                InputIterator labelsTo) {
            Entries.push_back(Entry(ident, author, date, comment, labelsFrom,
                    labelsTo));
        }

        int __fastcall GetCount() {
            return Entries.size();
        }

        UnicodeString __fastcall GetAuthor(int Index);
        UnicodeString __fastcall GetComment(int Index);
        _di_IStream __fastcall GetContent(int Index);
        TDateTime __fastcall GetDate(int Index);
        UnicodeString __fastcall GetIdent(int Index);
        TOTAHistoryStyle __fastcall GetHistoryStyle(int Index);
        int __fastcall GetLabelCount(int Index);
        UnicodeString __fastcall GetLabels(int Index, int LabelIndex);

        // 'IOTAFileHistory' methods.

        HRESULT __stdcall Get_Count(int &Count) OVERRIDE {
            Count = GetCount();
            return S_OK;
        }

        HRESULT __stdcall GetAuthor(int Index, WideString &Author) OVERRIDE {
            Author = GetAuthor(Index);
            return S_OK;
        }

        HRESULT __stdcall GetComment(int Index, WideString &Comment) OVERRIDE
        {
            Comment = GetComment(Index);
            return S_OK;
        }

        HRESULT __stdcall GetContent(int Index, _di_IStream &Content) OVERRIDE
        {
            Content = GetContent(Index);
            return S_OK;
        }

        HRESULT __stdcall GetDate(int Index, TDateTime &Date) OVERRIDE {
            Date = GetDate(Index);
            return S_OK;
        }

        HRESULT __stdcall GetIdent(int Index, WideString &Ident) OVERRIDE {
            Ident = GetIdent(Index);
            return S_OK;
        }

        HRESULT __stdcall GetHistoryStyle(int Index,
            TOTAHistoryStyle &HistoryStyle) OVERRIDE
        {
            HistoryStyle = GetHistoryStyle(Index);
            return S_OK;
        }

        HRESULT __stdcall GetLabelCount(int Index, int &LabelCount) OVERRIDE {
            LabelCount = GetLabelCount(Index);
            return S_OK;
        }

        HRESULT __stdcall GetLabels(int Index, int LabelIndex,
            WideString &Value) OVERRIDE
        {
            Value = GetLabels(Index, LabelIndex);
            return S_OK;
        }

        // 'IDispatch' methods.

        HRESULT __stdcall GetTypeInfoCount(UINT *pctinfo) OVERRIDE {
            return E_NOTIMPL;
        }

        HRESULT __stdcall GetTypeInfo(UINT iTInfo, LCID lcid,
            ITypeInfo **ppTInfo) OVERRIDE
        {
            return E_NOTIMPL;
        }

        HRESULT __stdcall GetIDsOfNames(REFIID riid, LPOLESTR *rgszNames,
            UINT cNames, LCID lcid, DISPID *rgDispId) OVERRIDE
        {
            return E_NOTIMPL;
        }

        HRESULT __stdcall Invoke(DISPID dispIdMember, REFIID riid, LCID lcid,
            WORD wFlags, DISPPARAMS *pDispParams, VARIANT *pVarResult,
            EXCEPINFO *pExcepInfo, UINT *puArgErr) OVERRIDE
        {
            return E_NOTIMPL;
        }

        // 'IUnknown' methods.
        INTFOBJECT_IMPL_IUNKNOWN(TInterfacedObject);

    private:
        struct Entry {
            UnicodeString Ident;
            UnicodeString Author;
            TDateTime Date;
            UnicodeString Comment;

            std::vector<UnicodeString> Labels;

            template<typename InputIterator>
            __fastcall Entry(const UnicodeString ident,
                    const UnicodeString author, const TDateTime date,
                    const UnicodeString comment, InputIterator labelsFrom,
                    InputIterator labelsTo)
                    : Ident(ident),
                      Author(author),
                      Date(date),
                      Comment(comment),
                      Labels(labelsFrom, labelsTo) {
            }
        };

        UnicodeString FFileName;
        _di_IHgVersionControlService FHg;
        std::vector<Entry> Entries;
    };
}

/*
 * THgFileHistoryProvider implementation.
 */

__fastcall THgFileHistoryProvider::THgFileHistoryProvider(
    _di_IHgVersionControlService Hg)
{
    if (!Hg) {
        throw EArgumentNilException(Rtlconsts_SArgumentNil);
    }
    FHg = Hg;
}

UnicodeString __fastcall THgFileHistoryProvider::GetIdent() {
    return _D("HgFileHistoryProvider");
}

UnicodeString __fastcall THgFileHistoryProvider::GetName() {
    return _D("HgFileHistoryProvider");
}

_di_IOTAFileHistory __fastcall THgFileHistoryProvider::GetFileHistory(
    const UnicodeString FileName)
{
    ShowDebugMessage(L"GetFileHistory: AFileName = " + QuotedStr(FileName));

    _di_IOTAFileHistory history;
    THgFileHistory *hgHistory = NULL;
    try {
        _di_IXMLNode log = FHg->GetFileHistory(FileName);
        if (log) {
            THgFileHistory *hgHistory = new THgFileHistory(FileName, FHg);
            hgHistory->GetInterface(history);
            assert(history);

            _di_IXMLNodeList entries = log->ChildNodes;
            for (int i = 0; i != entries->Count; ++i) {
                _di_IXMLNode entry = entries->Nodes[i];
                if (entry->LocalName == L"logentry") {
                    UnicodeString revision = entry->Attributes[L"revision"];

                    _di_IXMLNodeList fields = entry->ChildNodes;
                    _di_IXMLNode author = fields->FindNode(L"author");
                    _di_IXMLNode date = fields->FindNode(L"date");
                    _di_IXMLNode msg = fields->FindNode(L"msg");
                    if (author != 0 && date != 0 && msg != 0) {
                        vector<UnicodeString> tags;
                        for (int j = 0; j != fields->Count; ++j) {
                            if (fields->Nodes[j]->LocalName == L"tag")
                                tags.push_back(fields->Nodes[j]->NodeValue);
                        }

                        hgHistory->AddEntry(revision, author->NodeValue,
                                XMLTimeToDateTime(date->NodeValue, true),
                                msg->NodeValue, tags.begin(), tags.end());
                    }
                }
            }
        }

        if (!hgHistory->GetInterface(history)) {
            delete hgHistory;
        }
    } catch (EHgError &e) {
        ShowDebugMessage(L"GetFileHistory: Ignored EHgError: " +
                e.ToString()); // TODO: L18N
    }
    return history;
}

/*
 * THgFileHistory implementation.
 */

__fastcall THgFileHistory::THgFileHistory(const UnicodeString FileName,
        _di_IHgVersionControlService Hg) {
    if (!Hg) {
        throw EArgumentNilException(Rtlconsts_SArgumentNil);
    }
    FFileName = FileName;
    FHg = Hg;
}

UnicodeString __fastcall THgFileHistory::GetAuthor(int Index) {
    if (Index < 0 || Index >= GetCount()) {
        throw EArgumentOutOfRangeException(Rtlconsts_SArgumentOutOfRange);
    }
    return Entries[Index].Author;
}

UnicodeString __fastcall THgFileHistory::GetComment(int Index) {
    if (Index < 0 || Index >= GetCount()) {
        throw EArgumentOutOfRangeException(Rtlconsts_SArgumentOutOfRange);
    }
    return Entries[Index].Comment;
}

_di_IStream __fastcall THgFileHistory::GetContent(int Index) {
    if (Index < 0 || Index >= GetCount()) {
        throw EArgumentOutOfRangeException(Rtlconsts_SArgumentOutOfRange);
    }
    TStreamAdapter *adapter = new TStreamAdapter(
            FHg->Open(FileName, Entries[Index].Ident), soOwned);
    _di_IStream stream;
    if (!adapter->GetInterface(stream)) {
        delete adapter;
    }
    return stream;
}

TDateTime __fastcall THgFileHistory::GetDate(int Index) {
    if (Index < 0 || Index >= GetCount()) {
        throw EArgumentOutOfRangeException(Rtlconsts_SArgumentOutOfRange);
    }
    return Entries[Index].Date;
}

UnicodeString __fastcall THgFileHistory::GetIdent(int Index) {
    if (Index < 0 || Index >= GetCount()) {
        throw EArgumentOutOfRangeException(Rtlconsts_SArgumentOutOfRange);
    }
    return Entries[Index].Ident;
}

TOTAHistoryStyle __fastcall THgFileHistory::GetHistoryStyle(int Index) {
    if (Index < 0 || Index >= GetCount()) {
        throw EArgumentOutOfRangeException(Rtlconsts_SArgumentOutOfRange);
    }
    return hsRemoteRevision;
}

int __fastcall THgFileHistory::GetLabelCount(int Index) {
    if (Index < 0 || Index >= GetCount()) {
        throw EArgumentOutOfRangeException(Rtlconsts_SArgumentOutOfRange);
    }
    return Entries[Index].Labels.size();
}

UnicodeString __fastcall THgFileHistory::GetLabels(int Index, int LabelIndex) {
    // The range for Index will be checked in GetLabelCount().
    if (LabelIndex < 0 || LabelIndex >= GetLabelCount(Index)) {
        throw EArgumentOutOfRangeException(Rtlconsts_SArgumentOutOfRange);
    }
    return Entries[Index].Labels[LabelIndex];
}
