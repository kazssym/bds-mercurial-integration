{
  Hg - basic declarations
  Copyright (C) 2012-2014 Kaz Nishimura

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.
}

unit Hg;

interface

uses
  Classes, SysUtils, XMLIntf;

{$SCOPEDENUMS ON}

type
  { Base exception for this package. }
  EHgError = class(Exception);

  { Mercurial command error. }
  EHgCommandError = class(EHgError);

  THgFileStatus = (Unknown, Ignored, Clean, Modified, Added, Removed, Missing);

  THgFile = record
    Name: string;
    Status: THgFileStatus;
  end;

  THgFiles = array of THgFile;

  THgCommitOptions = record
    { Fields will be added later. }
  end;

  { Interface to a Mercurial repository. }
  IHgRepository = interface
    ['{E4276E78-0DA9-45DF-963F-818B68384CD7}']

    { Returns the absolute path to this repository. }
    function GetPath: string; stdcall;

    { Returns a list of all files in this repository. }
    function GetFiles(): THgFiles; stdcall;

    { Make a commit to the local repository. }
    procedure Commit(const Message: string; const FileNames: array of string;
        const Options: THgCommitOptions); stdcall;

    { Absolute path to this repository. }
    property Path: string read GetPath;
  end;

  { Master interface to the Mercurial version control. }
  IHgVersionControlService = interface
    ['{4ED86EC7-DFA2-4469-9581-170C5847C7C8}']

    {
      Returns the root path of the repository that contains a file.
    }
    function GetRepositoryPath(const FileName: string): string; stdcall;

    {
      Returns the repository that contains a file.
    }
    function GetRepository(const FileName: string): IHgRepository; stdcall;

    {
      Returns the status of a file.
    }
    function GetFileStatus(const FileName: string): THgFileStatus; stdcall;

    {
      Returns the history of a file.
    }
    function GetFileHistory(const FileName: string): IXMLNode; stdcall;

    {
      Returns the content of a revision of a file as a stream.
      The returned stream must be destroyed.
    }
    function Open(const FileName, Revision: string): TStream; stdcall;
  end;

implementation

end.
