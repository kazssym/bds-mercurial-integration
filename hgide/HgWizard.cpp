/*
 * HgWizard - wizard registration (implementation)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <basepch.h>
#pragma hdrstop

#include "HgWizard.h"

#include "HgStrings.hpp"
#include "HgFileHistory.h"
#include "HgVersionControl.h"
#include "HgUtils.h"
#include <memory>
#include <cassert>

using namespace std;

#pragma package(smart_init)

void __fastcall Hgwizard::Register() {
    _di_IOTAWizard wizard;
    new THgWizard()->GetInterface(wizard);
    assert(wizard);

    RegisterPackageWizard(wizard);
}

/*
 * THgWizard implementaion.
 */

__fastcall THgWizard::THgWizard() {
    FHgClientFactory = new THgClientFactory(NULL);
    PluginInfoIndex = -1;
    VersionControlNotifierIndex = -1;
    FileHistoryProviderIndex = -1;
}

__fastcall THgWizard::~THgWizard() {
    if (FileHistoryProviderIndex >= 0) {
        _di_IOTAFileHistoryManager fileHistoryManager;
        BorlandIDEServices->Supports(fileHistoryManager);
        assert(fileHistoryManager);

        fileHistoryManager->
            UnregisterHistoryProvider(FileHistoryProviderIndex);
    }
    if (VersionControlNotifierIndex >= 0) {
        _di_IOTAVersionControlServices versionControlServices;
        BorlandIDEServices->Supports(versionControlServices);
        assert(versionControlServices);

        versionControlServices->RemoveNotifier(VersionControlNotifierIndex);
    }
    if (PluginInfoIndex >= 0) {
        _di_IOTAAboutBoxServices aboutBoxServices;
        BorlandIDEServices->Supports(aboutBoxServices);
        assert(aboutBoxServices);

        aboutBoxServices->RemovePluginInfo(PluginInfoIndex);
    }

    delete FHgClientFactory;
}

void __fastcall THgWizard::AfterConstruction() {
    _di_IHgVersionControlService hg =
        FHgClientFactory->GetVersionControlService();

    _di_IOTAAboutBoxServices aboutBoxServices;
    if (BorlandIDEServices->Supports(aboutBoxServices)) {
        const TVarRec formatArgs[] = {
            _D("") PACKAGE_NAME,
            _D("") PACKAGE_VERSION,
            _D("2012-2014"),
        };
        UnicodeString title =
            UnicodeString::Format(Hgstrings_PluginTitle, formatArgs, 2);
        UnicodeString description =
            UnicodeString::Format(Hgstrings_PluginDescription, formatArgs, 2);
        UnicodeString licenseStatus = Hgstrings_PluginLicenseStatus;

        PluginInfoIndex = aboutBoxServices->
            AddPluginInfo(title, description, NULL, false, licenseStatus);
    }

    _di_IOTAVersionControlServices versionControlServices;
    if (BorlandIDEServices->Supports(versionControlServices)) {
        _di_IOTAVersionControlNotifier150 notifier;
        new THgVersionControlNotifier(hg)->GetInterface(notifier);
        assert(notifier);

        VersionControlNotifierIndex = versionControlServices->
            AddNotifier(notifier);
    }

    _di_IOTAFileHistoryManager fileHistoryManager;
    if (BorlandIDEServices->Supports(fileHistoryManager)) {
        _di_IOTAFileHistoryProvider provider;
        new THgFileHistoryProvider(hg)->GetInterface(provider);
        assert(provider);

        FileHistoryProviderIndex = fileHistoryManager->
            RegisterHistoryProvider(provider);
    }
}

UnicodeString __fastcall THgWizard::GetIDString() {
    return _D("HgWizard");
}

UnicodeString __fastcall THgWizard::GetName() {
    return _D("HgWizard");
}

TWizardState __fastcall THgWizard::GetState() {
    return TWizardState();
}

void __fastcall THgWizard::Execute() {
}

void __fastcall THgWizard::AfterSave() {
}

void __fastcall THgWizard::BeforeSave() {
}

void __fastcall THgWizard::Destroyed() {
}

void __fastcall THgWizard::Modified() {
}
