/*
 * THgCommitForm - commit form for HgBDS (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef THgCommitFormH
#define THgCommitFormH 1

#include "Hg.hpp"
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Controls.hpp>
#include <ActnList.hpp>
#include <Classes.hpp>

/**
 * Form for the Commit function.
 */
class THgCommitForm : public TForm {
__published:
    TPanel *Panel1;
    TLabel *Label1;
    TLabel *Label2;
    TListView *FileListView;
    TMemo *MessageEdit;
    TButton *OKButton;
    TButton *CancelButton;

    TActionList *ActionList1;
    TAction *FormOK;

    void __fastcall FormOKUpdate(TObject *Sender);

public:
    __fastcall THgCommitForm(TComponent *Owner);

    void __fastcall Clear();

    /**
     * Adds a file to the file list.
     */
    void __fastcall AddFile(THgFile &File, bool Selected = false);
};

extern PACKAGE THgCommitForm *HgCommitForm;

#endif
