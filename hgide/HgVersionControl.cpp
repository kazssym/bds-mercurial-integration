/*
 * HgVersionControl - Project Manager integration for HgBDS (implementation)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <basepch.h>
#pragma hdrstop

#include "HgVersionControl.h"

#include "HgStrings.hpp"
#include "THgStatusForm.h"
#include "HgClient.h"
#include "HgUtils.h"
#include <RTLConsts.hpp>
#include <Dialogs.hpp>
#include <IOUtils.hpp>
#include <boost/shared_ptr.hpp>
#include <cassert>

using namespace std;
using boost::shared_ptr;

/*
 * These macros are renamed in later C++Builder versions.
 */
#ifndef Rtlconsts_SArgumentNil
#define Rtlconsts_SArgumentNil System_Rtlconsts_SArgumentNil
#endif

#pragma package(smart_init)

namespace Hgversioncontrol {
    typedef void __fastcall (__closure *TExecuteEvent)(TObject *Sender,
        const _di_IInterfaceList MenuContextList);
    typedef bool __fastcall (__closure *TPreExecuteEvent)(TObject *Sender,
        const _di_IInterfaceList MenuContextList);
    typedef bool __fastcall (__closure *TPostExecuteEvent)(TObject *Sender,
        const _di_IInterfaceList MenuContextList);

    /*
     * Implements 'IOTAProjectManagerMenu'.
     */
    class TProjectManagerMenu : public TInterfacedObject,
            private IOTAProjectManagerMenu, private IOTALocalMenu {
        typedef TInterfacedObject inherited;

    public:
        __property TExecuteEvent OnExecute = {
            read = FOnExecute, write = FOnExecute,
        };
        __property TPreExecuteEvent OnPreExecute = {
            read = FOnPreExecute, write = FOnPreExecute,
        };
        __property TPostExecuteEvent OnPostExecute = {
            read = FOnPostExecute, write = FOnPostExecute,
        };

        /**
         * Constructs this object with the default property values.
         */
        __fastcall TProjectManagerMenu() {
            FEnabled = true;
        }

        // 'IOTAProjectManagerMenu' methods.

        bool __fastcall GetIsMultiSelectable() OVERRIDE {
            return FMultiSelectable;
        }

        void __fastcall SetIsMultiSelectable(bool Value) OVERRIDE {
            FMultiSelectable = Value;
        }

        void __fastcall Execute(const _di_IInterfaceList MenuContextList)
            OVERRIDE
        {
            if (FOnExecute) {
                FOnExecute(this, MenuContextList);
            }
        }

        bool __fastcall PreExecute(const _di_IInterfaceList MenuContextList)
            OVERRIDE
        {
            if (FOnPreExecute) {
                return FOnPreExecute(this, MenuContextList);
            }
            return true;
        }

        bool __fastcall PostExecute(const _di_IInterfaceList MenuContextList)
            OVERRIDE
        {
            if (FOnPostExecute) {
                return FOnPostExecute(this, MenuContextList);
            }
            return true;
        }

        // 'IOTALocalMenu' methods.

        UnicodeString __fastcall GetCaption() OVERRIDE {
            return FCaption;
        }

        bool __fastcall GetChecked() OVERRIDE {
            return FChecked;
        }

        bool __fastcall GetEnabled() OVERRIDE {
            return FEnabled;
        }

        int __fastcall GetHelpContext() OVERRIDE {
            return FHelpContext;
        }

        UnicodeString __fastcall GetName() OVERRIDE {
            return FName;
        }

        UnicodeString __fastcall GetParent() OVERRIDE {
            return FParent;
        }

        int __fastcall GetPosition() OVERRIDE {
            return FPosition;
        }

        UnicodeString __fastcall GetVerb() OVERRIDE {
            return FVerb;
        }

        void __fastcall SetCaption(const UnicodeString Caption) OVERRIDE {
            FCaption = Caption;
        }

        void __fastcall SetChecked(bool Checked) OVERRIDE {
            FChecked = Checked;
        }

        void __fastcall SetEnabled(bool Enabled) OVERRIDE {
            FEnabled = Enabled;
        }

        void __fastcall SetHelpContext(int HelpContext) OVERRIDE {
            FHelpContext = HelpContext;
        }

        void __fastcall SetName(const UnicodeString Name) OVERRIDE {
            FName = Name;
        }

        void __fastcall SetParent(const UnicodeString Parent) OVERRIDE {
            FParent = Parent;
        }

        void __fastcall SetPosition(int Position) OVERRIDE {
            FPosition = Position;
        }

        void __fastcall SetVerb(const UnicodeString Verb) OVERRIDE {
            FVerb = Verb;
        }

        // 'IOTANotifier' methods.

        void __fastcall AfterSave() OVERRIDE {
        }

        void __fastcall BeforeSave() OVERRIDE {
        }

        void __fastcall Destroyed() OVERRIDE {
        }

        void __fastcall Modified() OVERRIDE {
        }

        // 'IUnknown' methods.
        INTFOBJECT_IMPL_IUNKNOWN(TInterfacedObject);

    private:
        UnicodeString FCaption;
        bool FChecked;
        bool FEnabled;
        int FHelpContext;
        UnicodeString FName;
        UnicodeString FParent;
        int FPosition;
        UnicodeString FVerb;
        bool FMultiSelectable;

        TExecuteEvent FOnExecute;
        TPreExecuteEvent FOnPreExecute;
        TPostExecuteEvent FOnPostExecute;
    };
}

/*
 * 'THgVersionControlNotifier' implementation
 */

__fastcall THgVersionControlNotifier::THgVersionControlNotifier(
    _di_IHgVersionControlService Service)
{
    if (!Service) {
        throw EArgumentNilException(Rtlconsts_SArgumentNil);
    }
    FVersionControlService = Service;
}

_di_IOTAProjectManagerMenu __fastcall
    THgVersionControlNotifier::GetMercurialMenu()
{
    if (!FMercurialMenu) {
        TProjectManagerMenu *newMenu = new TProjectManagerMenu();
        newMenu->GetInterface(FMercurialMenu);
        assert(FMercurialMenu);

        FMercurialMenu->Caption = Hgstrings_MercurialMenuCaption;
        FMercurialMenu->Position = pmmpUserVersionControl;
        FMercurialMenu->Verb = _D("Mercurial");
        FMercurialMenu->IsMultiSelectable = true;
    }
    return FMercurialMenu;
}

_di_IOTAProjectManagerMenu __fastcall
    THgVersionControlNotifier::GetStatusMenu()
{
    if (!FStatusMenu) {
        TProjectManagerMenu *newMenu = new TProjectManagerMenu();
        newMenu->OnExecute = StatusMenuExecute;
        newMenu->GetInterface(FStatusMenu);
        assert(FStatusMenu);

        FStatusMenu->Caption = Hgstrings_StatusMenuCaption;
        FStatusMenu->Parent = MercurialMenu->Verb;
        FStatusMenu->Position = MercurialMenu->Position + 1;
        FStatusMenu->Verb = _D("MercurialStatus");
        FStatusMenu->IsMultiSelectable = true;
    }
    return FStatusMenu;
}

bool __fastcall THgVersionControlNotifier::CheckoutProject(
    UnicodeString &ProjectName)
{
    // TODO: Maybe clone.
    throw ENotImplemented(_D("") __func__ " not implemented");
}

bool __fastcall THgVersionControlNotifier::CheckoutProjectWithConnection(
    UnicodeString &ProjectName, const UnicodeString Connection)
{
    throw ENotImplemented(_D("") __func__ " not implemented");
}

UnicodeString __fastcall THgVersionControlNotifier::GetName() {
    return _D("Mercurial");
}

UnicodeString __fastcall THgVersionControlNotifier::GetDisplayName() {
    return _D("Mercurial");
}

bool __fastcall THgVersionControlNotifier::IsFileManaged(
    const _di_IOTAProject Project, TStrings *const IdentList)
{
    for (int i = 0; i != IdentList->Count; ++i) {
        UnicodeString ident = IdentList->Strings[i];
        if (TPath::IsPathRooted(ident) && TFile::Exists(ident)) {
            // Any file in a repository is considered to be managed.
            UnicodeString repositoryPath =
                    FVersionControlService->GetRepositoryPath(ident);
            return repositoryPath.Length() != 0;
        } else {
            ShowDebugMessage(_D("IsFileManaged: IdentList[") + IntToStr(i) +
                _D("] = ") + QuotedStr(ident));
        }
    }
    return false;
}

void __fastcall THgVersionControlNotifier::ProjectManagerMenu(
    const _di_IOTAProject Project, TStrings *const IdentList,
    const _di_IInterfaceList ProjectManagerMenuList, bool IsMultiSelect)
{
    ProjectManagerMenuList->Add(MercurialMenu);
    ProjectManagerMenuList->Add(StatusMenu);

    if (!UpdateMenu) {
        TProjectManagerMenu *menu1 = new TProjectManagerMenu();
        __try {
            if (menu1->GetInterface(UpdateMenu)) {
                menu1 = 0;
            }
            UpdateMenu->Caption = Hgstrings_UpdateCaption;
            UpdateMenu->Enabled = false;
            UpdateMenu->Parent = MercurialMenu->Verb;
            UpdateMenu->Position = MercurialMenu->Position + 2;
            UpdateMenu->Verb = L"MercurialUpdate";
        } __finally {
            delete menu1;
        }
    }
    ProjectManagerMenuList->Add(UpdateMenu);

#if 0
    for (int i = 0; i != ProjectManagerMenuList->Count; ++i) {
        _di_IOTAProjectManagerMenu menu;
        if (ProjectManagerMenuList->Items[i]->Supports(menu)) {
            ShowDebugMessage(_D("ProjectManagerMenu: Caption = ") +
                QuotedStr(menu->Caption) + _D(", Name = ") +
                QuotedStr(menu->Name) + _D(", Parent = ") +
                QuotedStr(menu->Parent) + _D(", Position = ") +
                IntToStr(menu->Position) + _D(", Verb = ") +
                QuotedStr(menu->Verb));
        }
    }
#endif
}

bool __fastcall THgVersionControlNotifier::AddNewProject(
    const _di_IOTAProject Project) {
    // TODO: Maybe init.
    throw ENotImplemented(_D("") __func__ " not implemented");
}

void __fastcall THgVersionControlNotifier::StatusMenuExecute(TObject *Sender,
    const _di_IInterfaceList MenuContextList)
{
    // Tries to save all files.
    _di_IOTAModuleServices moduleServices;
    if (BorlandIDEServices->Supports(moduleServices)) {
        bool toRetry;
        do {
            toRetry = false;
            if (!moduleServices->SaveAll()) {
                switch (MessageDlg(_D("Some files are not saved."),
                    mtInformation, mbAbortRetryIgnore, 0))
                {
                case mrAbort:
                    return;
                case mrRetry:
                    toRetry = true;
                    break;
                case mrIgnore:
                    break;
                }
            }
        } while (toRetry);
    }

    _di_IHgRepository repository;
    for (int i = 0; i != MenuContextList->Count; ++i) {
        _di_IOTAProjectMenuContext context;
        if (MenuContextList->Items[i]->Supports(context)) {
            UnicodeString ident = context->Ident;
            if (!repository) {
                repository = FVersionControlService->GetRepository(ident);
            } else {
                if (repository->Path !=
                    FVersionControlService->GetRepositoryPath(ident))
                {
                    MessageDlg(_D("Files must be in a single repository."),
                        mtWarning, TMsgDlgButtons() << mbOK, 0);
                    return;
                }
            }
        }
    }

    HgStatusForm = new THgStatusForm(NULL);
    try {
        HgStatusForm->Repository = repository;
        HgStatusForm->ShowModal();
    } catch (...) {
        delete HgStatusForm;
        throw;
    }
    delete HgStatusForm;
}
