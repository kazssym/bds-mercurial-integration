/*
 * HgClient - Mercurial client (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <basepch.h>
#pragma hdrstop

#include "HgClient.h"

#include "HgUtils.h"
#include <winsock2.h>
#include <RTLConsts.hpp>
#include <XMLDoc.hpp>
#include <IOUtils.hpp>
#include <boost/cstdint.hpp>
#include <vector>
#include <cassert>

using namespace std;
using boost::uint32_t;

/*
 * These macros are renamed in later C++Builder versions.
 */
#ifndef Rtlconsts_SArgumentOutOfRange
#define Rtlconsts_SArgumentOutOfRange System_Rtlconsts_SArgumentOutOfRange
#endif
#ifndef Rtlconsts_SArgumentNil
#define Rtlconsts_SArgumentNil System_Rtlconsts_SArgumentNil
#endif

#pragma package(smart_init)

namespace Hgclient {
    /*
     * Mercurial client that uses pipe streams.
     */
    class THgPipeClient : public THgClient {
        typedef THgClient inherited;

    public:
        static THgPipeClient *__fastcall Create(
                const UnicodeString Directory = UnicodeString()) {
            HANDLE sendHandles[2] = {
                INVALID_HANDLE_VALUE,
                INVALID_HANDLE_VALUE
            };
            HANDLE receiveHandles[2] = {
                INVALID_HANDLE_VALUE,
                INVALID_HANDLE_VALUE
            };
            try {
                SECURITY_ATTRIBUTES pipeAttributes = {
                    sizeof (SECURITY_ATTRIBUTES),
                };
                pipeAttributes.bInheritHandle = true;
                if (!CreatePipe(&sendHandles[0], &sendHandles[1],
                        &pipeAttributes, 0)) {
                    RaiseLastOSError();
                }
                if (!CreatePipe(&receiveHandles[0], &receiveHandles[1],
                        &pipeAttributes, 0)) {
                    RaiseLastOSError();
                }

                // These calls are not critical.
                SetHandleInformation(sendHandles[1], HANDLE_FLAG_INHERIT, 0);
                SetHandleInformation(receiveHandles[0], HANDLE_FLAG_INHERIT,
                        0);

                STARTUPINFOW startup = {
                    sizeof (STARTUPINFOW),
                };
                startup.dwFlags |= STARTF_USESTDHANDLES;
                startup.hStdInput = sendHandles[0];
                startup.hStdOutput = receiveHandles[1];
                startup.hStdError = GetStdHandle(STD_ERROR_HANDLE);

                // This must be writable.
                wchar_t commandLine[] =
                        L"hg serve --encoding UTF-8 --cmdserver pipe";
                const wchar_t *currentDirectory = NULL;
                if (Directory.Length() != 0) {
                    currentDirectory = Directory.c_str();
                }

                PROCESS_INFORMATION child;
                if (!CreateProcessW(NULL, commandLine, NULL, NULL, true,
                        DETACHED_PROCESS, NULL, currentDirectory, &startup,
                        &child)) {
                    RaiseLastOSError();
                }
                // These values are not used.
                CloseHandle(child.hThread);
                CloseHandle(child.hProcess);
            } catch (...) {
                if (receiveHandles[1] != INVALID_HANDLE_VALUE) {
                    CloseHandle(receiveHandles[1]);
                }
                if (receiveHandles[0] != INVALID_HANDLE_VALUE) {
                    CloseHandle(receiveHandles[0]);
                }
                if (sendHandles[1] != INVALID_HANDLE_VALUE) {
                    CloseHandle(sendHandles[1]);
                }
                if (sendHandles[0] != INVALID_HANDLE_VALUE) {
                    CloseHandle(sendHandles[0]);
                }
                throw;
            }
            CloseHandle(receiveHandles[1]);
            CloseHandle(sendHandles[0]);

            THgPipeClient *client = NULL;
            try {
                THandleStream *sendPipe = NULL;
                THandleStream *receivePipe = NULL;
                try {
                    sendPipe = new THandleStream(THandle(sendHandles[1]));
                    receivePipe =
                            new THandleStream(THandle(receiveHandles[0]));
                    client = new THgPipeClient(sendPipe, receivePipe,
                            TEncoding::UTF8, Directory);
                } catch (...) {
                    delete receivePipe;
                    delete sendPipe;
                    throw;
                }

                client->ExpectHello();
            } catch (...) {
                delete client;
                throw;
            }
            return client;
        }

        __fastcall THgPipeClient(THandleStream *SendStream,
                THandleStream *ReceiveStream, TEncoding *Encoding,
                const UnicodeString CurrentDirectory)
                : inherited(SendStream, ReceiveStream, Encoding) {
        }

        virtual __fastcall ~THgPipeClient() {
            FileClose(static_cast<THandleStream *>(SendStream)->Handle);
            FileClose(static_cast<THandleStream *>(ReceiveStream)->Handle);
            delete ReceiveStream;
            delete SendStream;
        }
    };

    /*
     * Implements 'IHgRepository' for the Mercurial client.
     */
    class THgClientRepository : public TInterfacedObject,
            private IHgRepository {
        typedef TInterfacedObject inherited;

    public:
        __fastcall THgClientRepository(const UnicodeString Path,
            THgClient *Client)
        {
            assert(TPath::IsPathRooted(Path));
            assert(TDirectory::Exists(Path));
            FPath = Path;
            FClient = Client;
        }

        // 'IHgRepository' methods.

        UnicodeString __stdcall GetPath() {
            return FPath;
        }

        THgFiles __stdcall GetFiles() {
            UnicodeString params[] = {
                _D("status"),
                _D("--cwd=") + FPath,
                _D("--all"),
            };

            THgFiles files;
            TStream *output = NULL;
            TStream *error = NULL;
            TTextReader *outputReader = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();
                if (FClient->RunCommand(params, 2, output, error) != 0) {
                    FClient->RaiseCommandError(error);
                }
                // Parses the output for files.
                output->Seek(0, 0);
                outputReader = new TStreamReader(output, FClient->Encoding,
                        false, 1024);
                while (outputReader->Peek() >= 0) {
                    UnicodeString line = outputReader->ReadLine();
                    if (line.Length() >= 2 && line[2] == _D(' ')) {
                        files.Length = files.Length + 1;
                        files[files.High].Name =
                                line.SubString(3, line.Length() - 2);
                        files[files.High].Status =
                                THgClient::ParseFileStatus(line);
                    }
                }
            } __finally {
                delete outputReader;
                delete error;
                delete output;
            }
            return files;
        }

        void __stdcall Commit(const UnicodeString Message,
            const UnicodeString *FileNames, const int FileNames_High,
            const THgCommitOptions Options) {
            UnicodeString paramsInit[] = {
                _D("commit"),
                _D("--cwd=") + FPath,
                _D("--message=") + Message,
            };
            vector<UnicodeString> params(paramsInit, paramsInit + 3);
            for (int i = 0; i != FileNames_High + 1; ++i) {
                params.push_back(FileNames[i]);
            }

            TStream *output = NULL;
            TStream *error = NULL;
            TTextReader *outputReader = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();
                if (FClient->RunCommand(&params[0], params.size() - 1,
                        output, error) != 0) {
                    FClient->RaiseCommandError(error);
                }
                // The following code may be unnecessary.
                output->Seek(0, 0);
                outputReader = new TStreamReader(output, FClient->Encoding,
                        false, 1024);
                while (outputReader->Peek() >= 0) {
                    UnicodeString line = outputReader->ReadLine();
                    ShowMessage(_D("hg commit: ") + line);
                }
            } __finally {
                delete outputReader;
                delete error;
                delete output;
            }
        }

        // 'IUnknown' methods.
        INTFOBJECT_IMPL_IUNKNOWN(inherited);

    private:
        UnicodeString FPath;
        THgClient *FClient;
    };

    /*
     * Implements 'IHgVersionControlService' for the Mercurial client.
     */
    class THgClientVersionControlService : public TInterfacedObject,
            private IHgVersionControlService {
        typedef TInterfacedObject inherited;

    public:
        __fastcall THgClientVersionControlService() {
            try {
                FClient = THgPipeClient::Create();
            } catch (EHgError &) {
                ShowDebugMessage(_D("")
                        "THgClientVersionControlService: got an error while "
                        "creating a client for the current directory");
            }
        }

        virtual __fastcall ~THgClientVersionControlService() {
            delete FClient;
        }

        // 'IHgVersionControlService' methods.

        UnicodeString __stdcall GetRepositoryPath(
                const UnicodeString FileName) {
            THgClient *client = GetClient(FileName);
            if (!client) {
                return UnicodeString();
            }

            UnicodeString directoryPath = TPath::GetFullPath(FileName);
            directoryPath = TPath::GetDirectoryName(directoryPath);
            assert(directoryPath.Length() != 0);

            UnicodeString repositoryPath;
            TStream *output = NULL;
            TStream *error = NULL;
            TStreamReader *outputReader = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();

                UnicodeString params[] = {
                    _D("root"), _D("--cwd=") + directoryPath
                };
                if (client->RunCommand(params, 1, output, error) == 0) {
                    output->Seek(0, 0);
                    outputReader = new TStreamReader(output, client->Encoding,
                            false, 1024);
                    repositoryPath = outputReader->ReadLine();
                } else {
                    ShowDebugMessage(_D("") __func__
                            ": got a Mercurial command error for" +
                            QuotedStr(FileName));
                }
            } __finally {
                delete outputReader;
                delete error;
                delete output;
            }
            return repositoryPath;
        }

        _di_IHgRepository __stdcall GetRepository(
                const UnicodeString FileName) {
            _di_IHgRepository repository;
            UnicodeString rootPath = GetRepositoryPath(FileName);
            if (rootPath.Length() != 0) {
                // FHgClient must be valid here.
                new THgClientRepository(rootPath, FClient)
                        ->GetInterface(repository);
                assert(repository);
            }
            return repository;
        }

        THgFileStatus __stdcall GetFileStatus(const UnicodeString FileName) {
            THgClient *client = GetClient(FileName);
            if (!client) {
                return THgFileStatus::Unknown;
            }

            THgFileStatus status = THgFileStatus::Unknown;
            TStream *output = NULL;
            TStream *error = NULL;
            TStreamReader *outputReader = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();

                UnicodeString params[] = {
                    _D("status"), _D("--all"), FileName
                };
                if (client->RunCommand(params, 2, output, error) == 0) {
                    output->Seek(0, 0);
                    outputReader = new TStreamReader(output, client->Encoding,
                            false, 1024);

                    UnicodeString line = outputReader->ReadLine();
                    status = THgClient::ParseFileStatus(line);
                } else {
                    ShowDebugMessage(_D("") __func__
                            ": got a Mercurial command error for" +
                            QuotedStr(FileName));
                }
            } __finally {
                delete outputReader;
                delete error;
                delete output;
            }
            return status;
        }

        _di_IXMLNode __stdcall GetFileHistory(const UnicodeString FileName) {
            THgClient *client = GetClient(FileName);
            if (!client) {
                return NULL;
            }

            _di_IXMLNode history;
            TStream *output = NULL;
            TStream *error = NULL;
            TStreamReader *outputReader = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();

                UnicodeString params[] = {
                    _D("log"), _D("--style=xml"), FileName
                };
                if (client->RunCommand(params, 2, output, error) == 0) {
                    output->Seek(0, 0);
                    outputReader = new TStreamReader(output, client->Encoding,
                            false, 1024);

                    UnicodeString xml = outputReader->ReadToEnd();
                    _di_IXMLDocument document = LoadXMLData(xml);
                    history = document->DocumentElement;
                } else {
                    ShowDebugMessage(_D("") __func__
                            ": got a Mercurial command error for" +
                            QuotedStr(FileName));
                }
            } __finally {
                delete outputReader;
                delete error;
                delete output;
            }
            return history;
        }

        TStream *__stdcall Open(const UnicodeString FileName,
                const UnicodeString Revision) {
            THgClient *client = GetClient(FileName);
            if (!client) {
                return NULL;
            }

            TStream *output = NULL;
            TStream *error = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();

                const UnicodeString params[] = {
                    _D("cat"), _D("-r"), Revision, FileName
                };
                if (client->RunCommand(params, 3, output, error) == 0) {
                    output->Seek(0, 0);
                } else {
                    ShowDebugMessage(_D("") __func__
                            ": got a Mercurial command error for" +
                            QuotedStr(FileName));
                    delete output;
                }
            } __finally {
                delete error;
            }
            return output;
        }

        // 'IUnknown' methods.
        INTFOBJECT_IMPL_IUNKNOWN(inherited);

    protected:
        THgClient *__fastcall GetClient(const UnicodeString FileName) {
            if (!FClient) {
                UnicodeString directoryPath = TPath::GetFullPath(FileName);
                directoryPath = TPath::GetDirectoryName(directoryPath);
                try {
                    FClient = THgPipeClient::Create(directoryPath);
                } catch (...) {
                    ShowDebugMessage(_D("")
                            "THgClientVersionControlService: got an error "
                            "while creating a client for " +
                            QuotedStr(directoryPath));
                }
            }
            return FClient;
        }

    private:
        THgClient *FClient;
    };
}

/*
 * THgClient implementation
 */

THgFileStatus __fastcall THgClient::ParseFileStatus(
        const UnicodeString StatusLine) {
    THgFileStatus status = THgFileStatus::Unknown;
    if (StatusLine.Length() >= 2 && StatusLine[2] == _D(' ')) {
        switch (StatusLine[1]) {
        case _D('C'):
            status = THgFileStatus::Clean;
            break;
        case _D('M'):
            status = THgFileStatus::Modified;
            break;
        case _D('A'):
            status = THgFileStatus::Added;
            break;
        case _D('R'):
            status = THgFileStatus::Removed;
            break;
        case _D('!'): // Missing or deleted unexpectedly
            status = THgFileStatus::Missing;
            break;
        case _D('I'):
            status = THgFileStatus::Ignored;
            break;
        }
    }
    return status;
}

__fastcall THgClient::THgClient(TStream *SendStream, TStream *ReceiveStream,
        TEncoding *Encoding)
        : FSendStream(SendStream),
          FReceiveStream(ReceiveStream),
          FEncoding(Encoding ? Encoding : TEncoding::Default) {
    if (!FSendStream || !FReceiveStream) {
        throw EArgumentNilException(Rtlconsts_SArgumentNil);
    }
}

void __fastcall THgClient::ExpectHello() {
    // Handles a hello message on the output channel.
    Byte channel;
    int length;
    ReceiveChannel(channel, length);
    if (channel != Byte('o')) {
        // TODO: Use more specific exception type.
        throw EHgError(_D("Protocol error"));
    }

    vector<Byte> hello(length);
    FReceiveStream->ReadBuffer(&hello.front(), hello.size());
    // TODO: Parse the hello message.
}

int __fastcall THgClient::RunCommand(const UnicodeString *Args,
        const int Args_High, TStream *OutputStream, TStream *ErrorStream) {
    if (Args_High < 0) {
        throw EArgumentOutOfRangeException(Rtlconsts_SArgumentOutOfRange);
    }

    DynamicArray<Byte> data;
    TStringBuilder *dataBuilder = NULL;
    __try {
        dataBuilder = new TStringBuilder(Args[0]);
        for (int i = 1; i != Args_High + 1; ++i) {
            dataBuilder->Append(L'\0');
            dataBuilder->Append(Args[i]);
        }
        data = FEncoding->GetBytes(dataBuilder->ToString());
    } __finally {
        delete dataBuilder;
    }
    FSendStream->WriteBuffer("runcommand\n", 11);
    SendData(&data[0], data.Length);

    // Receives channels until the command finishes.
    for (;;) {
        Byte channel;
        int length;
        ReceiveChannel(channel, length);

        if (islower((unsigned char)channel) && length >= 0) {
            vector<Byte> bytes(length);
            try {
                FReceiveStream->ReadBuffer(&bytes.front(), bytes.size());
            } catch (EReadError &e) {
                throw EHgError(e.ToString());
            }

            switch (channel) {
            case Byte('o'):
                if (OutputStream) {
                    OutputStream->WriteBuffer(&bytes.front(), bytes.size());
                }
                break;
            case Byte('d'):
            case Byte('e'):
                if (ErrorStream) {
                    ErrorStream->WriteBuffer(&bytes.front(), bytes.size());
                }
                break;
            case Byte('r'):
                if (bytes.size() == 4) {
                    uint32_t nResult =
                            reinterpret_cast<const uint32_t&>(bytes.front());
                    return ntohl(nResult);
                }
                throw EHgError(_D("Protocol error")); // TODO: L18N
            default:
                break;
            }
        } else {
            throw EHgError(_D("Unexpected channel")); // TODO: L18N
        }
    }
}

void __fastcall THgClient::RaiseCommandError(TStream *ErrorStream) {
    ErrorStream->Seek(0, 0);

    UnicodeString message;
    TStreamReader *errorReader = NULL;
    try {
        errorReader = new TStreamReader(ErrorStream, FEncoding, false, 1024);
        message = errorReader->ReadToEnd().TrimRight();
    } catch (...) {
        delete errorReader;
        throw;
    }
    throw EHgCommandError(message);
}

void __fastcall THgClient::SendData(const void *Data, int Length) {
    uint32_t nLength = htonl(Length);
    FSendStream->WriteBuffer(&nLength, 4);
    FSendStream->WriteBuffer(Data, Length);
}

void __fastcall THgClient::ReceiveChannel(Byte &Channel, int &Length) {
    try {
        FReceiveStream->ReadBuffer(&Channel, 1);

        uint32_t nLength;
        FReceiveStream->ReadBuffer(&nLength, 4);
        // The value of nLength is in the network byte order (big-endian).
        Length = ntohl(nLength);
    } catch (EReadError &e) {
        throw EHgError(e.ToString()); // TODO: L18N
    }
}

/*
 * THgClientFactory implementation
 */

__fastcall THgClientFactory::THgClientFactory(TComponent *Owner)
        : inherited(Owner) {
}

_di_IHgVersionControlService __fastcall
        THgClientFactory::GetVersionControlService() {
    _di_IHgVersionControlService service;
    new THgClientVersionControlService()->GetInterface(service);
    assert(service);
    return service;
}
