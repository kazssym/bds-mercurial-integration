/*
 * HgClient - Mercurial client (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HgClientH
#define HgClientH 1

#include "Hg.hpp"

namespace Hgclient {
    /*
     * Mercurial client.
     */
    class PACKAGE THgClient : public TObject {
        typedef TObject inherited;

    public:
        __property TEncoding *Encoding = {read = FEncoding};

        static THgFileStatus __fastcall ParseFileStatus(
                const UnicodeString StatusLine);

        /*
         * Constructs a THgClient object.
         * The default encoding will be used if Encoding is null.
         */
        __fastcall THgClient(TStream *SendStream, TStream *ReceiveStream,
                TEncoding *Encoding);

        void __fastcall ExpectHello();

        /*
         * Runs a command on the server.
         */
        int __fastcall RunCommand(const UnicodeString *Args,
                const int Args_High, TStream *OutputStream = NULL,
                TStream *ErrorStream = NULL);

        void __fastcall RaiseCommandError(TStream *ErrorStream);

    protected:
        __property TStream *SendStream = {read = FSendStream};
        __property TStream *ReceiveStream = {read = FReceiveStream};

        void __fastcall SendData(const void *Data, int Count);

        void __fastcall ReceiveChannel(Byte &Channel, int &Length);

    private:
        TStream *FSendStream;
        TStream *FReceiveStream;
        TEncoding *FEncoding;
    };

    /*
     * Factory class for Mercurial clients.
     */
    class PACKAGE THgClientFactory : public TComponent {
        typedef TComponent inherited;

    public:
        __fastcall THgClientFactory(TComponent *Owner);

        _di_IHgVersionControlService __fastcall GetVersionControlService();
    };
}
using namespace Hgclient;

#endif
