/*
 * Package configuration for HgBDS Mercurial Integration
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * Copying and distribution of this file, with or without modification, are
 * permitted in any medium without royalty provided the copyright notice and
 * this notice are preserved.  This file is offered as-is, without any
 * warranty.
 */

#define PACKAGE_NAME "HgBDS Mercurial Integration"
#define PACKAGE_VERSION "1.0.0"
#define PACKAGE_TAR_NAME "hgbds"
