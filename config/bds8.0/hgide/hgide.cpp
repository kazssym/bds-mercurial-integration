/*
 * Package entry point
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <basepch.h>
#pragma hdrstop
#pragma package(smart_init)

#include "../../../hgide/HgWizard.h"

// Package source.
//---------------------------------------------------------------------------

// Note: C++Builder XE does not invoke '_libmain'.

USEFORM("..\..\..\hgide\THgCommitForm.cpp", HgCommitForm);
USEFORM("..\..\..\hgide\THgStatusForm.cpp", HgStatusForm);
//---------------------------------------------------------------------------
#pragma argsused
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void*) {
    return 1;
}

