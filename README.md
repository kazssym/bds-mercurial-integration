This directory contains the source code for [HgBDS Mercurial Integration][].

HgBDS Mercurial Integration is a [RAD Studio][] plug-in to integrate basic
[Mercurial][] version control functions into the IDE.
It is designed to be compatible with RAD Studio XE, XE2, XE3, XE4, XE5, and
XE6, but support for XE and XE2 could be dropped soon unless there is
sufficient interest.

HgBDS Mercurial Integration is [free software][]: you can redistribute it
and/or modify it under the terms of the [GNU General Public License][].

For more information, visit <http://hgbds.vx68k.org/>.

[HgBDS Mercurial Integration]: <http://hgbds.vx68k.org/product>
[RAD Studio]: <http://www.embarcadero.com/products/rad-studio>
[Mercurial]: <http://mercurial.selenic.com/>
[Free software]: <http://www.gnu.org/philosophy/free-sw.html>
                 "What is free software?"
[GNU General Public License]: <http://www.gnu.org/licenses/gpl.html>

## Installation

Due to dependencies on Delphi run-time libraries, one package file are built
for each RAD Studio version as below.
You must choose the correct package file to use this program if you have got
pre-built package files.

| RAD Studio version | Package file name |
| ------------------ | ----------------- |
| XE (8.0)           | hgide150.bpl      |
| XE2 (9.0)          | hgide160.bpl      |
| XE3 (10.0)         | hgide170.bpl      |
| XE4 (11.0)         | hgide180.bpl      |
| XE5 (12.0)         | hgide190.bpl      |
| XE6 (14.0)         | hgide200.bpl      |
